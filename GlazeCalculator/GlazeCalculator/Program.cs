﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlazeCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // input

            double width, height, woodLength, glassArea;

            string widthString, heightString;

            Console.WriteLine("Width of window frame, in metres?");

            widthString = Console.ReadLine();

            Console.WriteLine("Height of window frame, in metres?");

            heightString = Console.ReadLine();

            /*
            Console.WriteLine("This is a {0} value: {1}", widthString.GetTypeCode(), widthString);
            Console.ReadLine();

            Console.WriteLine("This is a {0} value: {1}", heightString.GetTypeCode(), height);
            Console.ReadLine();
            */

            Console.ReadLine();

            // converting string inputs into numbers, using the number type 'double' and using the Parse method to convert string values to number values of type double

            width = double.Parse(widthString);

            height = double.Parse(heightString);

            /*
            Console.WriteLine("This is a {0} value: {1}" ,  width.GetTypeCode() , width );
            Console.ReadLine();

            Console.WriteLine("This is a {0} value: {1}", height.GetTypeCode(), height);
            Console.ReadLine();
            */

            woodLength = 2 * (width + height);

            glassArea = width * height;

            Console.WriteLine(" The length of the wood is " + woodLength + " metres");

            Console.WriteLine(" The area of glass is " + glassArea + " metres");

            Console.ReadLine();




        }
    }
}
